#!/bin/bash

apt update
apt install -y curl gpg ansible git

git clone https://gitlab.com/jptechnical/stackscripts.git

cd stackscripts
ansible-playbook debian12-linode.yml
ansible-playbook debian12-docker.yml
